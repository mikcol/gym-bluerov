from gymbluerov import BlueROVEnv
import numpy as np


def main():

    env = BlueROVEnv(marker=np.zeros(3),
                     dt=0.05,
                     tmax=30,
                     k1=1,
                     k2=1,
                     k3=1)
    env.reset()
    done = False

    while not done:

        action = env.action_space.sample()
        state, reward, done, _ = env.step(action)
    print(f'Final State: {state}, Final Reward: {reward}')


if __name__ == '__main__':

    main()
